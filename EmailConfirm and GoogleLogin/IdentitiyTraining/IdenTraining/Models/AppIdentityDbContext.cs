﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdenTraining.Models
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext() : base("MyTrainingDBContext") { }

        public static AppIdentityDbContext Create()
        {
            return new AppIdentityDbContext();
        }
    }
}