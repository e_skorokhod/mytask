﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdenTraining.Models
{
    public class AppUser:IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirthday { get; set; }
        public bool MaritalStatus { get; set; }
        public string Sex { get; set; }
        public DateTime RegistrationDate { get; set; } = DateTime.Now;

        public AppUser()
        {

        }
    }
}