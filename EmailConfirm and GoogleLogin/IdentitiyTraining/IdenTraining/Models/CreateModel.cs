﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IdenTraining.Models
{
    public class CreateModel
    {
        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Поле \"Логин\" обязательно для заполнения!")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 15 символов")]
        public string RegLogin { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Поле \"Имя\" обязательно для заполнения!")]
        public string RegFirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string RegLastName { get; set; }

        [Display(Name = "Пол")]
        [Required(ErrorMessage = "Поле \"Пол\" обязательно для заполнения!")]
        public string RegSex { get; set; }



        [Required(ErrorMessage = "Поле \"Возраст\" обязательно для заполнения!")]
        [Display(Name = "Возвраст")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime RegDateofBirthday { get; set; }

        [Required(ErrorMessage = "Поле \"Семейное положение\" обязательно для заполнения!")]
        [Display(Name = "Семейное положение")]
        public bool RegMaritalStatus { get; set; }

        [Required(ErrorMessage = "Поле \"Email\" обязательно для заполнения!")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string RegEmail { get; set; }

        [Display(Name = "Телефон")]
        public string RegPhone { get; set; }

        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Поле \"Пароль\" обязательно для заполнения!")]
        [DataType(DataType.Password)]
        [Compare("RegPasswordConfirm", ErrorMessage = "Пароли не совпадают")]
        public string RegPassword { get; set; }

        [Display(Name = "Подтверждение пароля")]
        [Required(ErrorMessage = "Поле \"Подтверждение пароля\" обязательно для заполнения!")]
        public string RegPasswordConfirm { get; set; }
    }
}