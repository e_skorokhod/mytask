﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IdenTraining.Models
{
    public class LoginModel
    {
        [Required]
        public string LogEmail { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string LogPassword { get; set; }
    }
}