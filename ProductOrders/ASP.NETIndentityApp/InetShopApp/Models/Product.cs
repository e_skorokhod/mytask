﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InetShopApp.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }


        public DateTime CreateDate { get; set; } = DateTime.Now;

        //Связь к промежуточной таблице с Orders
        public virtual ICollection<Order> Orders { get; set; }
        public Product()
        {
            Orders = new List<Order>();
        }
    }
}