﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InetShopApp.Models
{
    public class InetShopAppContext:IdentityDbContext<User>
    {
        public InetShopAppContext() : base("InetShopAppContext") { }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Log4NetLog> Log4NetLog { get; set; }

        public static InetShopAppContext Create()
        {
            return new InetShopAppContext();
        }

        public System.Data.Entity.DbSet<InetShopApp.Models.ApplicationRole> IdentityRoles { get; set; }
    }
}