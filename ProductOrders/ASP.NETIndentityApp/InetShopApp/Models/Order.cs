﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InetShopApp.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.Now;

        //Связь к таблице Users
       
        public virtual User User { get; set;}

        //Свзяь к промежуточной таблице с Products
        public virtual ICollection<Product> Products { get; set; }
        public Order()
        {
            Products = new List<Product>();
        }
    }
}