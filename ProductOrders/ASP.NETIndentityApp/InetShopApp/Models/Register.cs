﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InetShopApp.Models
{
    public class Register
    {
        [Required(ErrorMessage = "Поле Login обязательно для заполнения")]
        public string RegisterLogin { get; set; }

        [Required(ErrorMessage = "Поле Name обязательно для заполнения")]
        public string RegisterFirstName { get; set; }

        [Required(ErrorMessage = "Поле LastName обязательно для заполнения")]
        public string RegisterLastName { get; set; }

        [Required(ErrorMessage = "Поле Age обязательно для заполнения")]
        [Range(18, 60, ErrorMessage = "Значение поля возраст должно попадать в диапазон от 18 до 60")]
        public int RegisterAge { get; set; }

        [Required(ErrorMessage = "Поле Email обязательно для заполнения")]
        [RegularExpression(@"(?i)\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", ErrorMessage = "Email адрес указан не правильно")]
        public string RegisterEmail { get; set; }

        [Required(ErrorMessage = "Поле пароль обязательно для заполнения")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Следует указать пароль от 5 до 20 символов")]
        [Compare("RegisterPasswordConfirm", ErrorMessage = "Пароли не совпадают")]
        public string RegisterPassword { get; set; }

        
        public string RegisterPasswordConfirm { get; set; }

    }
}