﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetShopApp.Models
{
    public class ProductsCategoryVM
    {
        public IEnumerable<Product> Products { get; set; }
        public SelectList Categories { get; set; } = new SelectList(new List<string>() {"Все","Smartphones","Laptops", "Consoles","Tablets" });
        public string SelectedCategory { get; set; }

      
    }
}