﻿using InetShopApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace InetShopApp.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        InetShopAppContext db = new InetShopAppContext();

        //Список пользователей
        public ActionResult UsersList()
        {
            return View(db.Users.ToList());
        }

        //Детали пользователя
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }







        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Userslist");
            }
            return View(user);
        }








        //Удаление Пользователя////////////////////////////////////////////////
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }  
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("UsersList");
        }
        ///////////////////////////////////////////////////////////








        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(Login model)
        {
            if (ModelState.IsValid)
            {
               // User user = await UserManager.FindAsync(model.Email, model.Password);
                var user = UserManager.FindByEmail(model.Email);


                // var isMatch = UserManager.PasswordHasher.VerifyHashedPassword("AHcFNBKG2iOgRrdi0ml+C6LE0cU82elB7t+ptIlmG3/1UXW4jSkLpJNrnmX6vM5CZw==", "2261999");

                // if (user == null)
                if (!UserManager.CheckPassword(user, model.Password))
                {
                    ModelState.AddModelError("", "Неверный Email или пароль.");
                }
                else
                {
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    },claim);

                    var roles = UserManager.GetRoles(user.Id);
                    string areago = "";
                    string controllergo = "";
                    foreach (var b in roles)
                    {
                        if (b == "Admin")
                        {
                            areago = "Admin";
                            controllergo = "AdminHome";
                        }
                        else if (b == "User")
                        {
                            areago = "UserArea";
                            controllergo = "UserHome";
                        }
                    }
                    return RedirectToAction("Index", controllergo,new { area= areago });
                }
               
            }
            return View(model);
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult>Register(Register model)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    UserName = model.RegisterLogin,
                    FirstName = model.RegisterFirstName,
                    LastName = model.RegisterLastName,
                    Age = model.RegisterAge,
                    Email=model.RegisterEmail

                };
                
                IdentityResult result = await UserManager.CreateAsync(user, model.RegisterPassword);
                UserManager.AddToRole(user.Id, "User");
                if (result.Succeeded)
                {
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    foreach(string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model);
        }
        public ActionResult SignOut()
        {
            AuthenticationManager.SignOut();
            Session.Clear();
            return RedirectToAction("Home","Main");

        }
    }
}