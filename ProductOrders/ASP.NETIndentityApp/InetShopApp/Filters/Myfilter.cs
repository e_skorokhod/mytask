﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;


namespace InetShopApp.Filters
{
    public class Myfilter : Attribute, IExceptionFilter
    {
        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext,
              CancellationToken cancellationToken)
        {
            if (actionExecutedContext.Exception != null &&
                    actionExecutedContext.Exception is ArgumentException)
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                HttpStatusCode.BadRequest, "Цена не может быть меньше нуля!");

                Logger.InitLogger();//инициализация - требуется один раз в начале
                Logger.Log.Error($"FUCKING ERROR!");
            }
            return Task.FromResult<object>(null);
        }
        public bool AllowMultiple
        {
            get { return true; }
        }
    }
}