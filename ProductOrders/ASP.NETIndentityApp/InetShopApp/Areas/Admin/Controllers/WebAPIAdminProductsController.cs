﻿using InetShopApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace InetShopApp.Areas.Admin.Controllers
{
    public class WebAPIAdminProductsController : ApiController
    {
        InetShopAppContext db = new InetShopAppContext();

        public IHttpActionResult GetProducts()
        {

            return Json(db.Products.Select(x => new { x.ProductId, x.Name, x.Description, x.Category, x.Price }));
        }
 

        public IHttpActionResult GetProduct(int id)
        {
            Product product = db.Products.Find(id);
            return Json(new Product { ProductId = product.ProductId, Name=product.Name, Description = product.Description,Category=product.Category,Price=product.Price });
        }

        [HttpPost]
        public void CreateProduct([FromBody]Product product)
        {
            if (product.Price < 0)
            {
                throw new ArgumentException($"ЦЕНА ХУЕВАЯ!");
            }
            db.Products.Add(product);
            db.SaveChanges();
            Logger.InitLogger();//инициализация - требуется один раз в начале
            Logger.Log.Info($"New product: Name:{product.Name} Category:{product.Category} Price:{product.Price}");

        }

        [HttpPut]
        public void EditProduct(int id, [FromBody]Product product)
        {
           
            if (id == product.ProductId)
            {
                db.Entry(product).State = EntityState.Modified;

                db.SaveChanges();
            }
        }
       
        public void DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product != null)
            {
                db.Products.Remove(product);
                db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
