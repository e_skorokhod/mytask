﻿using InetShopApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace InetShopApp.Areas.Admin.Controllers
{
    public class AdminOrdersController : Controller
    {
        private InetShopAppContext db = new InetShopAppContext();

        // GET: Orders
        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.User).Include(x => x.Products);
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.User_Id = new SelectList(db.Users,"Id", "UserName");
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "Name");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string[] User_Id, int[] ProductId)
        {
            Order order = new Order();
            foreach (var a in ProductId)
            {
                order.Products.Add(db.Products.First(x => x.ProductId == a));
            }
            foreach(var b in User_Id)
            {
                order.User = db.Users.First(x => x.Id == b);
            }
           
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            //ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", order.User.Id);
            return View(order);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        //ДЕЛАТЬ
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            // Находим в бд футболиста
            Order order = db.Orders.Find(id);
            if (order != null)
            {
                // Создаем список команд для передачи в представление
                SelectList usersList = new SelectList(db.Users, "Id", "UserName", order.User.Id);
                SelectList productsList = new SelectList(db.Products, "ProductId", "Name");
                ViewBag.Users = usersList;
                ViewBag.Products = productsList;
                return View(order);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }


    }
}