﻿using InetShopApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetShopApp.Areas.UserArea.Controllers
{
    public class UserProductsController : Controller
    {
        private InetShopAppContext db = new InetShopAppContext();        
        public ActionResult Index()
        {
            ProductsCategoryVM plvm = new ProductsCategoryVM
            {
                SelectedCategory = "Все",
                Products = db.Products.ToList(),
            };
            return View(plvm);
        }
        [HttpPost]
        public ActionResult ProductCategorySearh(ProductsCategoryVM vm)
        {
            IEnumerable<Product> products = db.Products.ToList();
            if (!String.IsNullOrEmpty(vm.SelectedCategory) && !vm.SelectedCategory.Equals("Все"))
            {
                products = products.Where(p => p.Category == vm.SelectedCategory);
            }
            vm = new ProductsCategoryVM
            {
                SelectedCategory = vm.SelectedCategory,
                Products = products.ToList(),
            };
            return PartialView("ProductCategorySearh",vm);
        }

    }
}
