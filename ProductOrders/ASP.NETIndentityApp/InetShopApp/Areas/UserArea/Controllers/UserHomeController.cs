﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetShopApp.Areas.UserArea.Controllers
{
    public class UserHomeController : Controller
    {
        // GET: UserArea/UserHome
        public ActionResult Index()
        {
            return View();
        }
    }
}