﻿using InetShopApp.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InetShopApp.Areas.UserArea.Controllers
{
    public class CartController : Controller
    {
        private InetShopAppContext db = new InetShopAppContext();

        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnUrl = returnUrl
            });
        }


        public RedirectToRouteResult AddToCart(int? ProductId, string returnUrl)
        {

            Product product = db.Products.FirstOrDefault(p => p.ProductId == ProductId);
            if (product != null)
            {
                GetCart().AddItem(product, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(int ProductId, string returnUrl)
        {
            Product product = db.Products.FirstOrDefault(p => p.ProductId == ProductId);
            if (product != null)
            {
                GetCart().RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        
        public ActionResult Checkout()
        {
            Order order = new Order();
            Cart cart = (Cart)Session["Cart"];

            foreach (var vmP in cart.Lines)
            {
                order.Products.Add(db.Products.First(x => x.ProductId == vmP.Product.ProductId));
            }
            order.User = db.Users.Find(User.Identity.GetUserId());
            //order.Users = db.User.First(x => x.UserId == viewModel.UserId);
            db.Orders.Add(order);
            db.SaveChanges();
            Session.Clear();
            return View();

        }



        private Cart GetCart()
        {
            Cart cart = (Cart)Session["Cart"];
            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }
            return cart;
        }
    }
}

